{
  "title": "Salesforce | Horizontal Digital",
  "theme":"theme-dark-header",
  "meta": {
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle."
  },
  "og": {
    "title":"Salesforce | Horizontal Digital",
    "description":"An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle.",
    "url":"https://horizontaldigital.com/platforms",
    "sitename":"Horizontal Digital",
    "img":{
      "src":"https://hztl-fed.azureedge.net/hztl/images/HZTL_SocialImage.png"
    }
  },
  "components": [
    {
      "template":"components/section-col-single",
      "class":"theme-dark medium-bottom-margin",
      "content": [
        {
          "template":"components/rte",
          "content":"<div style=\"margin-bottom:40px;\"><img src=\"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/SalesforceLogo_180.png\" alt=\"Salesforce\"></div><h1 class=\"display-1\">We build Digital 360 experiences that drive richer relationships</h1><p>We believe organizations are only as strong as their worst customer experience. That’s why we elevate every moment across the digital journey to not only meet your customer’s needs...but also surpass them.</p><p>This experience-forward mentality demands that every action we drive on behalf of your business aligns with this promise and builds customer relationships that endure.</p><p>We deliver on this focus by designing and implementing website, ecommerce, and marketing programs powered by analytics & AI to create frictionless, personalized conversations for customers across all departments and channels.</p><p>Our global Salesforce practitioners are rooted in decades of digital legacy to not only implement best-in-class digital solutions, but also to deliver personalized, frictionless customer experiences at every touchpoint. All to enable us to deliver the Digital 360 model for some of the world’s most recognized Fortune 500 companies.</p>"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"theme-lite medium-bottom-margin",
      "title":"Custom solutions that maximize your Salesforce investment",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>We build connected experiences — fueled by strategic marketing programs and powered by analytics & AI — to address your customers’ needs. This fusion of sales, service, marketing and ecommerce data across every customer touchpoint simply powers more meaningful customer moments.</p>"
        },
        {
          "template":"components/component-cards",
          "items": [
            {
              "title":"Marketing Cloud",
              "content":"<p>Certified Level II Marketing Cloud specialists with product certifications to raise your marketing experiences.</p>",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/MarketingCloudIcon.png",
                "alt":"Marketing Cloud"
              },
              "link":{
                "url":"/platforms/salesforce/marketing-cloud",
                "text":"Learn more"
              }
            },
            {
              "title":"Commerce Cloud",
              "content":"<p>Digital Commerce leader with expertise in ecomm experiences that go beyond a sale.</p>",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/CommerceCloudIcon.png",
                "alt":"Commerce Cloud"
              },
              "link":{
                "url":"/platforms/salesforce/commerce-cloud",
                "text":"Learn more"
              }
            },
            {
              "title":"Experience Cloud",
              "content":"<p>Top Experience Cloud partner with deep CMS experience and custom innovations that further digital experiences.</p>",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/ExperienceCloudIcon.png",
                "alt":"Experience Cloud"
              },
              "link":{
                "url":"/platforms/salesforce/experience-cloud",
                "text":"Learn more"
              }
            },
            {
              "title":"Analytics & AI",
              "content":"<p>Certified Tableau CRM and Einstein specialists to deliver real-time experiences that anticipate needs.</p>",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/AnalyticsIcon.png",
                "alt":"Analytics & AI"
              },
              "link":{
                "url":"/platforms/salesforce/analytics-ai",
                "text":"Learn more"
              }
            },
            {
              "title":"Cross-Cloud & System Integrations",
              "content":"<p>Legacy leader in integrating cross-cloud and external systems that deliver connected data across sales and service.</p>",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/CrossCloudIcon.png",
                "alt":"Cross-Cloud & System Integrations"
              }
            }
          ]
        }
      ]
    },
    {
      "template":"components/section-wide-media",
      "class":"theme-lite medium-bottom-margin",
      "video":{
        "poster":{
          "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/Salesforce_Video_Thumb.jpeg",
          "alt":"Salesforce Video"
        },
        "id":"368877570"
      }
    },
    {
      "template":"components/section-col-single",
      "class":"theme-lite medium-bottom-margin",
      "title":"Your complex challenges, our complete expertise",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Our deep, cross-cloud knowledge of the Salesforce platform creates seamless customer engagements that translate into greater insights and opportunities.</p>"
        },
        {
          "template":"components/component-data-tile",
          "items":[
            {"content":"<div class=\"display-1\">275+</div><div class=\"display-4\">Cross Cloud Certifications</div>"},
            {"content":"<div class=\"display-1\">420%</div><div class=\"display-4\">Practice Growth in 2019</div>"},
            {"content":"<div class=\"display-1\">50+</div><div class=\"display-4\">Certified Team Members</div>"},
            {"content":"<div class=\"display-2\">Community Cloud</div><div class=\"display-4\">Advisory Board Member</div>"},
            {"content":"<div class=\"display-1\">4.7 CSAT</div><div class=\"display-4\">Rating on App Exchange</div>"},
            {"content":"<div class=\"display-2\">Salesforce Tenent</div><div class=\"display-4\">Alliance Member</div>"},
            {"content":"<div class=\"display-1\">1%</div><div class=\"display-4\">Pledge Partner</div>"},
            {"content":"<div class=\"display-1\">6</div><div class=\"display-4\">Custom Product Innovations on App Exchange</div>"}
          ]
        }
      ]
    },
    {
      "template":"components/section-logo-grid",
      "class":"theme-lite medium-bottom-margin",
      "text":"Industry Expertise",
      "filters":[
        {"tag":"Finacial","text":"Financial Services"},
        {"tag":"HealthCare","text":"Healthcare & Life Sciences"},
        {"tag":"Manufacturing","text":"Manufacturing"},
        {"tag":"Other","text":"Other"}
      ],
      "items":[
        {
          "tags":["Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/3M.png",
          "alt":"3M"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/AdvisoryBoard.png",
          "alt":"Advisory Board"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Allianze.png",
          "alt":"Allianze"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Allina.png",
          "alt":"Allina"
        },
        {
          "tags":["Featured","Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/AmericanStandard_Logo.png",
          "alt":"American Standard"
        },
        {
          "tags":["Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/AndersenWindows.png",
          "alt":"Andersen Windows"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/BCBS_LA.png",
          "alt":"Blue Cross Blue Shield of Louisiana"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/BostonScientific.png",
          "alt":"Boston Scientific"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Bremer.png",
          "alt":"Bremer"
        },
        {
          "tags":["Featured","Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Cat_Logo.png",
          "alt":"CAT"
        },
        {
          "tags":["Featured","Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Caribou_Logo.png",
          "alt":"Caribou Coffee"
        },
        {
          "tags":["Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Cargill.png",
          "alt":"Cargill"
        },
        {
          "tags":["Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Cenex_logo.png",
          "alt":"Cenex"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/ChildrensHealth_logo.png",
          "alt":"Children's Health"
        },
        {
          "tags":["Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/CHS.png",
          "alt":"CHS"
        },
        {
          "tags":["Manufacturing","Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Cushman.png",
          "alt":"Cushman & Wakefield"
        },
        {
          "tags":["Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Daltile_logo.png",
          "alt":"Daltile"
        },
        {
          "tags":["Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Deluxe.png",
          "alt":"Deluxe"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/EncompassHealth_logo.png",
          "alt":"Encompass Health"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/FederalReserve.png",
          "alt":"Federal Reserve Bank"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/First_Tech.png",
          "alt":"First Tech"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/FisherInvestments.png",
          "alt":"Fisher Investments"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Foresters.png",
          "alt":"Foresters Financial"
        },
        {
          "tags":["Featured","Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Formica_Logo.png",
          "alt":"Formica"
        },
        {
          "tags":["Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/GCI.png",
          "alt":"GCI"
        },
        {
          "tags":["Featured","Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/HardRock_Logo.png",
          "alt":"Seminole Hard Rock Support Services"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/HoraceMann_Logo.png",
          "alt":"Horace Mann"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/MayoHealth_logo.png",
          "alt":"Mayo Clinic"
        },
        {
          "tags":["Featured","Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/MIA_logo.png",
          "alt":"Minneapolis Institute of Art"
        },
        {
          "tags":["Featured","Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Milwaukee_Logo.png",
          "alt":"Milwaukee Tool"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/MUSC.png",
          "alt":"MUSC"
        },
        {
          "tags":["Featured","Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/NorthwesternMutual_Logo.png",
          "alt":"Northwestern Mutual"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/NewYorkLife.png",
          "alt":"New York Life"
        },
        {
          "tags":["Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/OlivetUniversity_logo.png",
          "alt":"Olivet University"
        },
        {
          "tags":["Featured","Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/PODS_Logo.png",
          "alt":"PODS"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Raymond_James.png",
          "alt":"Raymond James"
        },
        {
          "tags":["Other"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Salesforce_logo.png",
          "alt":"Salesforce"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Starkey_Hearing_Tech.png",
          "alt":"Starkey Hearing Technology"
        },
        {
          "tags":["Featured","Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/SubZero_Logo.png",
          "alt":"SubZero Wolf"
        },
        {
          "tags":["Featured","Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Toro_Logo.png",
          "alt":"Toro"
        },
        {
          "tags":["Finacial"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/TCW_logo.png",
          "alt":"TCW"
        },
        {
          "tags":["Featured","HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/UHG_Logo.png",
          "alt":"UnitedHealth Group"
        },
        {
          "tags":["HealthCare"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/UnitedHealth_logo.png",
          "alt":"UnitedHealthcare"
        },
        {
          "tags":["Manufacturing"],
          "src":"https://hztl-fed.azureedge.net/hztl/images/logos/Xrite.png",
          "alt":"X-rite Pantone"
        }
      ]
    },
    {
      "template":"components/section-big-quote",
      "class":"theme-dark medium-bottom-margin",
      "size":"display-3",
      "quote":"<p>The Horizontal Digital team members were tremendously valuable and critical to achieve our project objectives. The unique combination of technical expertise, tech/data architecture consultation and tight project management enabled us to be on the constant path towards our finish line. I would highly recommend Horizontal!</p>",
      "cite":"Product Owner, Deluxe | Erin Rowles",
      "img":{
        "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/Deluxe_logo.png",
        "alt":"Deluxe Logo"
      }
    },
    {
      "template":"components/section-col-half",
      "class":"theme-lite medium-bottom-margin",
      "cols":{
        "left":"col-md-6",
        "right":"col-md-6"
      },
      "title":"We create top-to-bottom engagements that make Digital 360 experiences work",
      "img":{
        "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/ProcessVennDiagram.png",
        "alt":"End-to-end experience diagram"
      },
      "content": {
        "left": [
          {
            "template": "components/rte",
            "content": "<p>Our cross-cloud expertise is only half the equation. Our Salesforce experts work hand-in-hand with our team of creative strategists and UX designers to build cohesive experiences that meet customers with the right message, at the right moment. This experience-forward approach is simply the most effective way to modernize and grow your Salesforce investment.</p>"
          }
        ]
      }
    },
    {
      "template":"components/section-col-half",
      "class":"theme-dark medium-bottom-margin",
      "cols":{
        "left":"col-md-8",
        "right":"col-md-4"
      },
      "img":{
        "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/47CSAT.jpeg",
        "alt":"4.7 CSAT"
      },
      "content": {
        "left": [
          {
            "template": "components/rte",
            "content": "<h3 class=\"display-1\">Our approach to long-term success</h3><p>Our industry-leading 4.7/5 CSAT score isn't for nothing. As digital leaders, we know the key to successful Salesforce investments go beyond technical functionalities — it's about understanding business challenges, defining success metrics, marrying user experiences with technical capabilities, delivering through adaptive iterations, and driving actionable insights to optimize results. We ensure all of our Salesforce implementations are right the first time around, are built for future scalability, and supported with training and enablement for customer independence.</p>"
          }
        ]
      }
    },
    {
      "template":"components/section-col-single",
      "class":"theme-lite collapse-bottom-margin",
      "title":"Our leaders and champions",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Located across nine global offices spanning the U.S., UAE, India and Malaysia, our international team of 65 certified Salesforce consultants hold over 275 cross-cloud certifications and operate with one distinct mission: Create Salesforce experiences that continuously elevate expectations.</p>"
        }
      ]
    },
    {
      "template":"components/section-quote-carousel",
      "class":"theme-lite",
      "slides":[
        {
          "quote":"<p>The Experience-Forward mindset is our DNA. It shapes everything we do.</p>",
          "size":"display-1",
          "cite":"CEO, Minneapolis, MN | Sabin Ephrem",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/SabinEphrem_CEO.png",
            "alt":"Sabin Ephrem"
          }
        },
        {
          "quote":"<p>Salesforce enables our clients to keep up with their changing customer needs.</p>",
          "size":"display-1",
          "cite":"Founder and EVP, Minneapolis, MN | Chris Staley",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/cstaley.png",
            "alt":"Chris Staley"
          }
        },
        {
          "quote":"<p>I’m driven by our commitment to put people at the center of everything we do.</p>",
          "size":"display-1",
          "cite":"President North American Digital, Minneapolis, MN | Julie Koepsell",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/JulieKoepsell.png",
            "alt":"Julie Koepsell"
          }
        },
        {
          "quote":"<p>Experience-Forward isn’t an option. It’s a requirement.</p>",
          "size":"display-1",
          "cite":"Vice President, Digital, Minneapolis, MN | Dave Michela",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/DaveMichela.png",
            "alt":"Dave Michela"
          }
        },
        {
          "quote":"<p>The all-time best is seeing clients turn digital transformation into a reality.</p>",
          "size":"display-1",
          "cite":"Vice President, Salesforce, Minneapolis, MN | Jill Marchand",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/JillMarchand.png",
            "alt":"Jill Marchand"
          }
        },
        {
          "quote":"<p>Salesforce’s constant quest to innovate and grow inspires me to grow with them.</p>",
          "size":"display-1",
          "cite":"Marketing Champion, Minneapolis, MN | Jackie Mennie",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/JackieMennie.png",
            "alt":"Jackie Mennie"
          }
        },
        {
          "quote":"<p>There’s no greater reward than seeing our clients succeed.</p>",
          "size":"display-1",
          "cite":"Director, Strategy, Minneapolis, MN | Stephanie Hogan",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/StephanieHogan.png",
            "alt":"Stephanie Hogan"
          }
        },
        {
          "quote":"<p>I am honored to found the new Jaipur Marketing Cloud User Group.</p>",
          "size":"display-1",
          "cite":"Managing Director, India, Jaipur, India | Anil Pilania",
          "img":{
            "src":"https://hztl-fed.azureedge.net/hztl/images/people/AnilPilania.png",
            "alt":"Anil Pilania"
          }
        }
      ]
    },
    {
      "template":"components/section-callout",
      "class":"theme-peach",
      "title":"Interested in learning more about our Salesforce expertise?",
      "content":"<p>Our team is here to help achieve your goals. Let's talk.</p>",
      "link":{
        "url":"https://horizontaldigital.com/contact-us",
        "text":"Contact Us"
      }
    },
    {
      "template":"components/section-footer-cta",
      "class":"theme-lite",
      "items": [
        {
          "type":"insights",
          "tag":"Insights",
          "title":"Salesforce Digital 360",
          "content":"Our experience-forward mindset perfectly aligns with Salesforce’s evolving platform.",
          "link":{
            "url":"https://www.horizontaldigital.com/insights/salesforce-digital-360"
          }
        },
        {
          "type":"work",
          "tag":"Work",
          "title":"Formica",
          "content":"Reimagining an iconic brand from the customer up.",
          "link":{
            "url":"https://www.horizontaldigital.com/work/formica"
          }
        }
      ]
    }
  ]
}
