// toggle see more button
const addActive = 'active';
const toggleBtn = '.toggle-btn--see-more';
const closeWindow = '.close-window';
const cardCta = '.card-list-card';

document.addEventListener(
  'click',
  (e) => {
    if (
      !e.target.matches(`${toggleBtn}, ${closeWindow}`) ||
      e.target.tagName === 'button'
    )
      return;
    if (e.target.matches(toggleBtn))
      return e.target.closest(cardCta).classList.add(addActive);
    e.target.closest(cardCta).classList.remove(addActive);
  },
  { passive: true }
);
