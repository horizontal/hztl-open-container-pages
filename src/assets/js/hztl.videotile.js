;(function($){

  var HztlVideoTile = function($elem){

    var config = {
      selectors: {
        playBtn:'a',
        overlay:'#video-overlay',
        video:'#video'
      },
      classes: {}
    }

    var $container,$overlay,$video,$btn;

    /*
     * Listeners
     */

    var handlePlayBtn = function(e){
      e.preventDefault();

      var vimeoId = $(this).attr('data-vimeoId');
      setPlayerUrl(vimeoId);

      $overlay.modal('show');
    }

    var handleOverlayClose = function(e){
      clearPlayerUrl();
    }

    /*
     * Setup Funcitons
     */



    /*
     * Utility
     */

     var setPlayerUrl = function(vimeoId){
       var url = 'https://player.vimeo.com/video/'+vimeoId+'?badge=0&autoplay=1';
       console.log(url)
       $video.attr('src',url);
     }

     var clearPlayerUrl = function(){
       $video.removeAttr('src');
     }

    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
      $overlay = $(config.selectors.overlay);
      $btn = $(config.selectors.playBtn,$container);
    }

    var setUp = function(){}

    var attachListeners = function(){
      if($overlay.length){
        $video = $(config.selectors.video,$overlay);
        $btn.on('click',handlePlayBtn);

        $overlay.on('hidden.bs.modal',handleOverlayClose);
      }
    }

    return init();

  }

  $(document).ready(function(){
    $('.card-video').each(function(){
      new HztlVideoTile( $(this) );
    });
  });

})(jQuery);
