;(function($){

  var FloatingCta = function($elem){

    var config = {
      selectors: {
        button:'.btn-cta'
      },
      classes: {
        show:"show"
      }
    }

    var $btn,$header,$body;

    /*
     * Listeners
     */

    var handleToggle = function(e){}

    /*
     * Setup Funcitons
     */

    var createScrollTrigger = function(){
      ScrollTrigger.create({
        start: "top -100px",
        end: "bottom bottom",
        toggleClass: {className: config.classes.show, targets: $container[0] }
      });
    }

    /*
     * Utility
     */



    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
      $btn = $(config.selectors.button,$container);
    }

    var setUp = function(){
      createScrollTrigger();
    }

    var attachListeners = function(){}

    return init();

  }

  $(document).ready(function(){
    $('.cta-floating').each(function(){
      new FloatingCta(  $(this) );
    });
  });

})(jQuery);
