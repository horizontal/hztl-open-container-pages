module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ["airbnb"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parserOptions: {
    ecmaFeatures: {},
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: [],
  rules: {
    "max-len": ["error", { code: 1000, tabWidth: 2 }],
  },
};
